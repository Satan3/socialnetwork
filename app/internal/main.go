package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os/signal"
	"socialNetwork/internal/config"
	"socialNetwork/internal/router"
	"syscall"
	"time"
)

func main() {
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	appConfig, err := config.LoadConfig("../")
	if err != nil {
		log.Fatal("Ошибка чтения конфигурационного файла:", err.Error())
	}

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", appConfig.AppPort),
		Handler: router.SetupRouter(),
	}

	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Listen: %s\n", err)
		}
	}()

	<-ctx.Done()

	stop()
	log.Println("Shutting down gracefully, press Ctrl+C again to force")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Server shutdown:", err)
	}

	log.Println("Server exiting")

}
