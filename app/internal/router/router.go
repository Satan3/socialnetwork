package router

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func SetupRouter() http.Handler {
	router := gin.Default()

	router.GET("/hello", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "Hello World")
	})

	router.GET("/test", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "test")
	})

	return router
}
